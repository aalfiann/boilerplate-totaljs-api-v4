/* global ROUTE */

'use strict';

exports.install = function () {
  ROUTE('/test/middleware', ['get', '#authorize']);
  ROUTE('/test/schema', ['post', '*Test_route --> @render', '#authorize']);
};
