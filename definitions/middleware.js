/* global F MIDDLEWARE CONF */

'use strict';

const handler = require(F.path.definitions('handler'));

MIDDLEWARE('authorize', function ($) {
  if ($.controller.req.headers['x-token'] && $.controller.req.headers['x-token'] === CONF.x_token) {
    $.next();
  } else {
    handler.error($, 'Wrong header token!');
  }
});
