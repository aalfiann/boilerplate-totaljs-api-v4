/* global CONF */

'use strict';

const mongoose = require('mongoose');
const cachegoose = require('recachegoose');
const MongooseSchema = mongoose.Schema;

switch (CONF.mongo_cache_engine.toLowerCase()) {
  case 'redis' :
    cachegoose(mongoose, { engine: 'redis', client: require('redis').createClient(CONF.mongo_cache_redis_conn) });
    break;
  case 'file' :
    cachegoose(mongoose, { engine: 'file' });
    break;
  default :
    cachegoose(mongoose);
}

/**
 * Transform document object
 * @param {*} doc
 * @param {*} ret
 * @return {object}
 */
function transform (doc, ret) {
  ret._id = doc._id.toString();
  return ret;
}

/**
 * Create Mongoose schema
 * @param {object} obj
 * @return {schema}
 */
function createSchema (obj) {
  const schema = new MongooseSchema(obj, {
    toObject: { transform },
    toJSON: { transform }
  });

  schema.set('toObject', { virtuals: true });
  return schema;
}

/**
 * Set Mongoose model
 * @param {string} name
 * @param {object} schema
 * @return {model}
 */
function setModel (name, schema) {
  return mongoose.model(name, createSchema(schema));
}

/**
 * Response error in Mongoose
 * @param {controller} $        this is the totaljs controller
 * @param {object} err          this is the error detail from Mongoose
 * @return {callback}
 */
function errorHandler ($, err) {
  $.controller.status = 400;
  const error = {
    code: err.code,
    message: err.errmsg,
    status: 'error',
    error: {
      driver: err.driver,
      name: err.name,
      index: err.index,
      keyPattern: err.keyPattern,
      keyValue: err.keyValue
    }
  };
  $.callback(error);
}

/**
 * Clear Cache
 * @param {string} key
 * @param {callback} cb
 */
function clearCache (key, cb) {
  return cachegoose.clearCache(key, cb);
}

module.exports = {
  createSchema,
  setModel,
  errorHandler,
  clearCache,
  mongoose
};
