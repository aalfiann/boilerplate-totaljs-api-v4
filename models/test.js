/* global F NEWSCHEMA */
'use strict';

const handler = require(F.path.definitions('handler'));

NEWSCHEMA('Test_route', function (schema) {
  schema.define('description', 'string', true);

  handler.schemaErrorBuilder('custom');
  schema.setError((error) => { error.setTransform('custom'); });

  schema.addWorkflow('render', function ($) {
    const data = $.model;
    handler.success($, 'Successful rendered!', { data: data.description });
  });
});
